package id.chalathadoa.challengechapter5.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import id.chalathadoa.challengechapter5.R
import id.chalathadoa.challengechapter5.databinding.FragmentSplashScreenBinding
import kotlinx.coroutines.delay

class FragmentSplashScreen : Fragment() {
    private var _binding: FragmentSplashScreenBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentSplashScreenBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        lifecycleScope.launchWhenCreated {
            splashScreenTimer()
            findNavController().navigate(R.id.action_fragmentSplashScreen_to_fragmentRegister)
        }
    }
    private suspend fun splashScreenTimer(){
        delay(2000)
    }
}