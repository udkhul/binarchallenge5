package id.chalathadoa.challengechapter5.fragment

import android.os.Bundle
import android.util.Patterns
import android.util.Patterns.EMAIL_ADDRESS
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.util.PatternsCompat.EMAIL_ADDRESS
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseAuth
import id.chalathadoa.challengechapter5.R
import id.chalathadoa.challengechapter5.databinding.FragmentRegisterBinding
import java.util.regex.Pattern

class FragmentRegister : Fragment() {
    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    //inisialisasi firebase
    private lateinit var auth: FirebaseAuth

    private var username = binding.etMasukkanUsernameRegister.text.toString().trim()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        auth = FirebaseAuth.getInstance()
        btnRegis()
    }

    private fun btnRegis(){
        binding.btnSubmitRegister.setOnClickListener {
            val email = binding.etMasukkanEmailRegister.text.toString().trim()
            val password = binding.etMasukkanPasswordRegister.text.toString().trim()
            val confirmPsw = binding.etKonfirmasiPasswordRegister.text.toString().trim()

            if (email.isEmpty()){
                binding.etMasukkanEmailRegister.error = "Email harus diisi"
                binding.etMasukkanEmailRegister.requestFocus()
                return@setOnClickListener
            }
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                binding.etMasukkanEmailRegister.error = "Email tidak valid"
                binding.etMasukkanEmailRegister.requestFocus()
                return@setOnClickListener
            }
            if (password.isEmpty() || password.length < 6){
                binding.etMasukkanPasswordRegister.error = "Minimal panjang password 6 karakter"
                binding.etMasukkanPasswordRegister.requestFocus()
                return@setOnClickListener
            }
            if (confirmPsw != password){
                binding.etKonfirmasiPasswordRegister.error = "Konfirmasi Pasword harus sama"
                binding.etKonfirmasiPasswordRegister.requestFocus()
                return@setOnClickListener
            }
            
            regisUser(email, password)
        }
    }

    private fun regisUser(email: String, password: String) {
        auth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(requireActivity()){
                if (it.isSuccessful){
                    findNavController().navigate(R.id.action_fragmentRegister_to_fragmentHome)
                } else {
                    Toast.makeText(requireContext(), it.exception?.message, Toast.LENGTH_SHORT).show()
                }
            }
    }
}